Name:           ink
Version:        0.5.3
Release:        1%{?dist}
Summary:        Ink is a program to display the ink level of printers

Group:          System Environment/Daemons 
License:        GPL
URL:            http://ink.sourceforge.net/
Source0:        ink-%{version}.tar.gz 
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: libinklevel-devel
Requires:      libinklevel

%description
Ink is a program to display the ink level of printers. It makes use of libinklevel.

%prep
%setup -q -n %{name}-%{version}

%build
%configure --prefix=/usr
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT install

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%{_bindir}/ink
%doc
%{_mandir}/man1/*.1*

%changelog
